#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
The setup script.
"""

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('HISTORY.md') as history_file:
    history = history_file.read()

requirements = [] #['Click>=6.0', ]

with open("requirements.txt") as requirements_file:
    requirements_lines = requirements_file.read().split('\n')
    requirements_list = [str(line) for line in requirements_lines
                         if not line.lstrip().startswith("#")
                         and line is not '']

    requirements += requirements_list

setup_requirements = ['pytest-runner', ]

test_requirements = ['pytest', ]

setup(
    author="Ali Rathore",
    author_email='ali.rathore@oracle.com',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
    ],
    description="Tools for Testing Apache Spark",
   # entry_points={
   #     'console_scripts': [
   #         'spark-on-oci=spark_on_oci.cli:main',
   #     ],
   # },
    install_requires=requirements,
    license="Apache Software License 2.0",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='spark-locust',
    name='spark-locust',
    packages=find_packages(),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.com/sparkless/spark-locust',
    version='0.1.0',
    zip_safe=False,
)

