import os

TEST_SOURCES=os.environ["TEST_SOURCES"]
EXPECTED_RESULTS=os.environ["EXPECTED_RESULTS"]
TEST_OUTPUTS=os.environ["TEST_OUTPUTS"]

THRIFTSERVER_WEB_URL= os.getenv("THRIFTSERVER_WEB_URL",None)
THRIFTSERVER_JDBC_URL= os.environ["THRIFTSERVER_JDBC_URL"]

SCREENSHOTS = os.getenv("SCREENSHOTS", 1)
BENCHMARK = os.getenv("BENCHMARK", 0)
EXPECTED = os.getenv("EXPECTED", True)

import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
