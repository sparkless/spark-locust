#!/bin/bash

set -e
set -x

ALLURE_RESULTS=${TEST_OUTPUTS}/allure-results
ALLURE_REPORTS=${TEST_OUTPUTS}/allure-reports
ALLURE_PORT=${REPORT_PORT}

mkdir -p ${ALLURE_REPORTS}/history

echo -e "\n\n Serving Test Report on port ${ALLURE_PORT}"
echo -e "\n Hit CTRL-C to shutdown"

cp -r ${ALLURE_REPORTS}/history ${ALLURE_RESULTS}/
allure generate ${ALLURE_RESULTS} -o ${ALLURE_REPORTS} --clean

unset DISPLAY
allure open ${ALLURE_REPORTS} -p ${ALLURE_PORT}
