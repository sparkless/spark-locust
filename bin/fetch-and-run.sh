#!/usr/bin/env bash

set -x
set -e

export TEST_SOURCES_REPO=${TEST_SOURCES_REPO:-}
export TEST_SOURCES_REPO_DIR=${TEST_SOURCES_REPO_DIR:-}
export TEST_FILES_REPO=${TEST_FILES_REPO:-}
export TEST_FILES_REPO_FILES=${TEST_FILES_REPO_FILES:-}
export EXPECTED_RESULTS_REPO=${EXPECTED_RESULTS_REPO:-}
export EXPECTED_RESULTS_REPO_DIR=${EXPECTED_RESULTS_REPO_DIR:-}
export TEST_OUTPUTS_BUCKET=${TEST_OUTPUTS_BUCKET:-}
export TEST_OUTPUTS_OBJECT=${TEST_OUTPUTS_OBJECT:-spark-${SPARK_VERSION}-${TEST_SOURCES_REPO}/${TEST_SOURCES_DIR}}
export GENERATE_REPORT=${GENERATE_REPORT:-true}

mkdir -p ${TEST_OUTPUTS} ${TEST_SOURCES} ${EXPECTED_RESULTS} ${TEST_FILES}

env

if [[ ! -z ${TEST_OUTPUTS_BUCKET} && -z $(ls -A ${TEST_OUTPUTS}) ]]; then
    echo "** Fetching previous Test Ouputs object ${TEST_OUTPUTS_OBJECT} from bucket ${TEST_OUTPUTS_BUCKET} into ${TEST_OUTPUTS} **"

    [[ -e ${HOME}/.oci/oci_api_key.pem ]] && oci setup repair-file-permissions --file ${HOME}/.oci/oci_api_key.pem || true
    [[ -e ${HOME}/.oci/config ]] && oci setup repair-file-permissions --file ${HOME}/.oci/config || true

    oci os object get --bucket-name ${TEST_OUTPUTS_BUCKET} --file /tmp/test-outputs.tar --name ${TEST_OUTPUTS_OBJECT} \
        && tar xzvf /tmp/test-outputs.tar \
        && cp -r test-outputs/* ${TEST_OUTPUTS}/ \
        || echo "Previous Test Outputs not found"
fi

if [[ ! -z ${TEST_SOURCES_REPO} && -z $(ls -A ${TEST_SOURCES}) ]]; then
    echo "** Cloning test sources from ${TEST_SOURCES_REPO} **"
    git clone ${TEST_SOURCES_REPO} /tmp/test-sources-repo
    cp -r /tmp/test-sources-repo/${TEST_SOURCES_REPO_DIR}/* ${TEST_SOURCES}
fi

if [[ ! -z ${EXPECTED_RESULTS_REPO} && -z $(ls -A ${EXECTED_RESULTS}) ]]; then
  echo "** Cloning test files from  ${EXPECTED_RESULTS_REPO} **"
    git clone ${EXPECTED_RESULTS_REPO} /tmp/expected-results-repo
    cp -r /tmp/expected-results-repo/${EXPECTED_RESULTS_REPO_DIR} ${EXPECTED_RESULTS}
fi

if [[ ! -z ${TEST_FILES_REPO} && -z $(ls -A ${TEST_FILES}) ]]; then
    echo "** Cloning test files from ${TEST_FILES_REPO} **"
    git clone ${TEST_FILES_REPO} /tmp/test-files-repo
    cp -r /tmp/test-files-repo/* ${TEST_FILES}
fi

echo "** Running Pytest whith arguments $@ **"

mkdir -p ${TEST_OUTPUTS}/screenshots

cd ${TEST_FILES}
pytest ${TEST_FILES_REPO_FILES}
cd -

if [[ ! -z ${GENERATE_REPORT} ]];then

    ALLURE_RESULTS=${TEST_OUTPUTS}/allure-results
    ALLURE_REPORTS=${TEST_OUTPUTS}/allure-reports

    mkdir -p ${ALLURE_REPORTS}/history

    echo -e "Generating new Allure Report"

    cp -r ${ALLURE_REPORTS}/history ${ALLURE_RESULTS}/

    allure generate ${ALLURE_RESULTS} -o ${ALLURE_REPORTS} --clean

fi

if [[ ! -z ${TEST_OUTPUTS_BUCKET} ]]; then
    echo "** Posting new Test Outputs to same bucket and object**"
    cd ${TEST_OUTPUTS}/..
    rm /tmp/test-outputs.tar || true
    tar  cvzf /tmp/test-outputs.tar test-outputs
    cd -
    oci os object put --force --bucket-name ${TEST_OUTPUTS_BUCKET} --file /tmp/test-outputs.tar --name ${TEST_OUTPUTS_OBJECT}
fi

if [[ ! -z ${REPORT_PORT} ]]; then
    unset DISPLAY
    ALLURE_PORT=${REPORT_PORT}
    allure open ${ALLURE_REPORTS} -p ${ALLURE_PORT}
fi
