#!/bin/bash

#set -x

CMD="bin/fetch-and-run.sh $@"
#CMD=/bin/bash

source .env

echo -e "\n\nRunning spark-locust with following configuration\n"
echo -e "THRIFTSERVER_WEB_URL\t${THRIFTSERVER_WEB_URL}"
echo -e "THRIFTSERVER_JDBC_URL\t${THRIFTSERVER_JDBC_URL}"
echo -e "TEST_FILES\t\t${TEST_FILES}"
echo -e "TEST_SOURCES\t\t${TEST_SOURCES}"
echo -e "EXPECTED_RESULTS\t${EXPECTED_RESULTS}"
echo -e "SCREENSHOTS\t\t${SCREENSHOTS}"
echo -e "BENCHMARK\t\t${BENCHMARK}"
echo -e "EXPECTED_RESULTS_REPO\t${EXPECTED_RESULTS_REPO}"
echo -e "TEST_FILES_REPO\t${TEST_FILES_REPO}"
echo -e "TEST_SOUCRES_REPO\t${TEST_SOURCES_REPO}"
echo -e "TEST_OUTPUTS_BUCKET\t${TEST_OUTPUTS_BUCKET}"

docker run -it --rm --name spark-locust \
	-w /spark-locust \
	-v ${PWD}:/spark-locust \
	-v ${HOME}/.oci:/home/spark-locust/.oci \
	-v ${EXPECTED_RESULTS}:/expected-results \
	-v ${TEST_OUTPUTS}:/test-outputs \
	-v ${TEST_FILES}:/test-files \
	-v ${TEST_SOURCES}:/test-sources \
	-e TEST_OUTPUTS=/test-outputs \
	-e TEST_SOURCES=/test-sources \
	-e TEST_FILES=/test-files \
	-e EXPECTED_RESULTS=/expected-results \
	-e THRIFTSERVER_WEB_URL \
	-e THRIFTSERVER_JDBC_URL \
	-e TEST_SOURCES_REPO \
	-e TEST_SOURCES_REPO_DIR \
	-e TEST_FILES_REPO \
	-e TEST_FILES_REPO_FILES \
	-e EXPECTED_RESULTS_REPO \
	-e EXPECTED_RESULTS_REPO_DIR \
	-e TEST_OUTPUTS_BUCKET \
	-e TEST_OUTPUTS_OBJECT \
	-e REPORT_PORT \
	-e PYTEST_ADDOPTS="--splinter-webdriver=chrome --splinter-headless=true --splinter-screenshot-dir=/test-outputs/screenshots --alluredir=/test-outputs/allure-results --benchmark-autosave --benchmark-storage=file:///test-outputs/benchmarks" \
    -e SCREENSHOTS \
    -e BENCHMARK \
	-p ${REPORT_PORT}:${REPORT_PORT} \
	${IMAGE} \
    ${CMD} 
