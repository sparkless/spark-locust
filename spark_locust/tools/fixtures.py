import pytest
import config
from config import logger
import os, re
import allure
from pyhive import hive
from spark_locust.tools import screenshots


@allure.step("Connecting to {url}")
def create_connection(url):
    host,port,database = parse_jdbc_url(url)
    conn = hive.Connection(host=host, port=int(port), database=database)
    return conn

@allure.step("Closing connection to {url}")
def close_connection(conn, url=config.THRIFTSERVER_JDBC_URL):
    conn.close()

def parse_jdbc_url(jdbc_url:str):

    try:
        jdbc_url = jdbc_url.replace('https://','').replace('http://','').rstrip('/')
        host,port = jdbc_url.split(':')
        parts = jdbc_url.split('/')
        database = parts[-1] if len(parts) > 1 else "default"
    except Exception as e:
        raise RuntimeError("Unable to parse JDBC connection string")
    else:
        return host,port,database

@pytest.fixture(scope='session')
def connection():
    logger.info(f">>> Opening connection to {config.THRIFTSERVER_JDBC_URL}")
    conn = create_connection(config.THRIFTSERVER_JDBC_URL)
    yield conn
    logger.info("<<< Closing connection")
    close_connection(conn, config.THRIFTSERVER_JDBC_URL)


import pandas
@allure.step("Running query \n {stmt}")
def run_query(connection,stmt):
    result = pandas.read_sql(stmt, connection)
    return result

@pytest.fixture(scope='function')
def execute(session_browser
            ,splinter_screenshot_dir
            ,request
            ,data_regression
            ,benchmark
            ,connection):


    def run(name, query, do_benchmark=False, do_screenshot=True,check_expected=True,display_sql_result=True):

        result = None
        logger.info(f"Running statement {name}")

        if check_expected:
            result = run_query(connection,stmt=query)

        if do_screenshot and config.SCREENSHOTS and config.THRIFTSERVER_WEB_URL:
            try:
                ss = screenshots.Screenshotter(session_browser, config.THRIFTSERVER_WEB_URL, splinter_screenshot_dir, name)
                ss.completed_job()
                ss.completed_query()
            except Exception as e:
                logger.warning(f"Error while trying to take screenshot: {e}")

        if check_expected and config.EXPECTED:
            expected(request,data_regression).check(result)

        if do_benchmark and config.BENCHMARK:
            benchmark(run_query, connection, stmt=query)

        if display_sql_result:
            print("""
            SQL Results
            -----------
            """)
            print(result)

    return run

@allure.step("Checking Expected Result")
def expected(request, data_regression):

    basename = re.sub(r"[\W]", "_", request.node.name)

    result_path = os.path.join(config.EXPECTED_RESULTS,request.node.module.__name__, f'{basename}expected.yaml')

    class _result_checker(object):

        def check(self,result):
            data_regression.check(result.to_dict(), fullpath=result_path)

    return _result_checker()
