from allure_commons.types import AttachmentType
import config
from config import logger
import allure
import os

thriftserver_components = {
    'jobs': {
        'params' : {

        },
        'xpaths' : {
            "completed_jobs" : "/html/body//table[@id='completedJob-table']/tbody/tr//td[1]",
            'failed_jobs' : "/html/body//table[@id='failedJob-table']/tbody/tr//td[1]",
            'running_jobs' : "/html/body//table[@id='activeJob-table']/tbody/tr//td[1]"
        }
    },
    'jobs/job': {
        'params' : {
            'id' : int
        },
        'xpaths': {
            'job' : '/html/body/div[2]'
        }
    },
    'SQL' : {
        'params' : {


        },
        'xpaths' : {
            'completed_sql' : "/html/body//table[@id='completed-execution-table']/tbody/tr//td[1]"
        }
    },
    'SQL/execution' : {
        'params' : {
            'id' : int
        },
        'xpaths': {
            'query' : "/html/body/div[2]"
        }
    }
}

def get_element(browser, page, params={}, xpath=None):

    url = config.THRIFTSERVER_WEB_URL.rstrip('/') + '/' + page.lstrip('/')

    urlparams = '&'.join([f'{k}={v}' if k in thriftserver_components[page]['params'] else '' for (k, v) in params.items()])
    if urlparams:
        url = url + '?' + urlparams

    browser.visit(url)

    if xpath:
        xpath = thriftserver_components[page]['xpaths'][xpath]
        try:
            return browser.find_by_xpath(xpath)
        except Exception as e:
            raise e

def get_queries(browser):
    """
    Get list of SQL page IDs
    :param browser:
    :return:
    """
    sqls = get_element(browser=browser,page='SQL',xpath='completed_sql')
    for s in sqls:
        qid = s.value
        yield int(qid)

def get_query(browser,qid):
    """
    Get SQL/execution page by id
    :param browser:
    :param id:
    :return:
    """
    query = get_element(browser=browser,page='SQL/execution',params={'id':qid}, xpath='query')
    return query

def get_jobs(browser):
    """
    Get list of (id, name) from completed jobs page
    :param browser:
    :return:
    """
    jobs = get_element(browser=browser,page='jobs',xpath='completed_jobs')
    for j in jobs:
        jid, name = j.value.split(' ')
        name = name.lstrip('(').rstrip(')')
        yield int(jid), name

def get_job(browser,jid):
    job = get_element(browser=browser,page='jobs/job',params={'id':jid},xpath='job')
    return job

def screenshot_last_completed_job(browser):
    jid, last_job_name = next(get_jobs(browser))
    job = get_job(browser,jid=jid)
    img = take_screenshot(job)
    return jid, img

def screenshot_last_completed_query(browser):
    qid = next(get_queries(browser))
    query = get_query(browser,qid=qid)
    img = take_screenshot(query)
    return qid, img

def take_screenshot(element, full=True):
    """
    Take a screeshot of a Splinter Element into filename
    :param element:
    :param full:
    :return:
    """
    if full:
        element.parent.full_screen()
    target = element.screenshot_as_png()
    element.parent.recover_screen()
    return target



class Screenshotter(object):

    def __init__(self, browser, url, dir, name):
        self.url = url.rstrip('/') + '/'
        self.browser = browser
        self.dir = dir
        self.name = name

    def take_screenshot(self, path, name):
        file = os.path.join(self.dir, name + '.png')
        path = self.url + path.lstrip('/')
        logger.info(f"Taking screenshot {name} to file {file} from url {path}")
        self.browser.visit(path)
        self.browser.full_screen()
        self.browser.driver.get_screenshot_as_file(file)
        self.browser.recover_screen()

    def completed_job(self):
        jid, img = screenshot_last_completed_job(self.browser)
        name = f'{self.name}-completed-job-{jid}.png'
        file = os.path.join(self.dir, name)
        img.save(file)
        allure.attach.file(file, name=name, attachment_type=AttachmentType.PNG)

    def completed_query(self):
        qid, img = screenshot_last_completed_query(self.browser)
        name = f'{self.name}-completed-query-{qid}.png'
        file = os.path.join(self.dir, name)
        img.save(file)
        allure.attach.file(file, name=name, attachment_type=AttachmentType.PNG)

#######################
# Patch for running chrome in docker
#
#  We have to add
#  options.add_argument('--no-sandbox')
#######################
from selenium.webdriver import Chrome
from splinter.driver.webdriver.chrome import WebDriver, WebDriverElement
from selenium.webdriver.chrome.options import Options
from splinter.driver.webdriver.cookie_manager import CookieManager
import functools
from pytest_splinter.plugin import _wait_for_condition, _visit

# Copied from pytest_splinter/plugin.py
def NoSandboxChromeBrowser(*args, **kwargs):
    """Emulate splinter's Browser."""
    visit_condition = kwargs.pop('visit_condition')
    visit_condition_timeout = kwargs.pop('visit_condition_timeout')
    browser = NoSandboxWebDriver(*args, **kwargs)
    browser.switch_to = browser.driver.switch_to
    browser.wait_for_condition = functools.partial(_wait_for_condition, browser)
    if hasattr(browser, 'driver'):
        browser.visit_condition = visit_condition
        browser.visit_condition_timeout = visit_condition_timeout
        browser.visit = functools.partial(_visit, browser, browser.visit)
    browser.__splinter_browser__ = True
    return browser

# Copied from splinter/driver/webdriver/chrome.py
class NoSandboxWebDriver(WebDriver):

    driver_name = "Chrome"

    def __init__(
        self,driver_name='chrome',
        options=None,
        user_agent=None,
        wait_time=2,
        fullscreen=False,
        incognito=False,
        headless=False,
        **kwargs
    ):

        options = Options() if options is None else options

        if user_agent is not None:
            options.add_argument("--user-agent=" + user_agent)

        if incognito:
            options.add_argument("--incognito")

        if fullscreen:
            options.add_argument("--kiosk")

        if headless:
            options.add_argument("--headless")
            options.add_argument("--disable-gpu")

        options.add_argument('--no-sandbox')

        self.driver = Chrome(options=options, **kwargs)

        self.element_class = WebDriverElement

        self._cookie_manager = CookieManager(self.driver)

        super(WebDriver, self).__init__(wait_time)
