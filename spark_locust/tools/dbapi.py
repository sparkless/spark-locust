import time, os
import snap_lab

from locust import Locust, TaskSet, events, task

class DBApiClient(snap_lab.SNAPClient):
    """
    Wrapper around SQL client
    """
    def sql(self, query, *args, **kwargs):

        start_time = time.time()
        try:
            result = super().sql(query, *args, **kwargs)
            if result == None:
                raise RuntimeError(f"No results for query: {query}")

        except Exception as e:
            total_time = int((time.time() - start_time) * 1000)
            events.request_failure.fire(request_type="sql", name=query, response_time=total_time, exception=e)
        else:
            total_time = int((time.time() - start_time) * 1000)
            events.request_success.fire(request_type="sql", name=query, response_time=total_time, response_length=len(result))
            return result

        return None

class DBApiLocust(Locust):
    """
    This is the abstract Locust class which should be subclassed. It provides an SQL client
    that can be used to make SQL requests that will be tracked in Locust's statistics.
    """

    def __init__(self, *args, **kwargs):
        super(DBApiLocust, self).__init__(*args, **kwargs)
        self.client = DBApiClient(self.host,port=self.port)

class TPCDSTaskSet(TaskSet):

    @task()
    def get_tables(self):
        self.client.sql("show tables in tpcds_100")

    @task()
    def get_databases(self):
        self.client.sql("show databases")

class DBApiUser(DBApiLocust):
    host = os.environ.get("THRIFTSERVER_HOST","localhost")
    port= int(os.environ.get("THRIFTSERVER_PORT","10000"))
    min_wait = 100
    max_wait = 1000
    task_set = TPCDSTaskSet