import os

class TestSources(object):

    def __init__(self, source_dir):
        """
        Wrapper object that encapsulates the generation/retrieval of
        SQL statements.

        Each method of this class returns a generator that returns a
        pair of (file-name, sql-statement

        :param source_dir: The directory containing the SQL statements
        """
        self.test_source_dir = source_dir
        self.create_table_dir = os.path.join(source_dir, 'create_table')
        self.sql_dir = os.path.join(source_dir, 'sql')
        self.create_star_schema_dir = os.path.join(source_dir, 'create_star_schema')
        self.create_olap_index_dir = os.path.join(source_dir, 'create_olap_index')
        self.readme_file = os.path.join(source_dir, 'README.md')

    def _get_statement(self, directory, files):

        if isinstance(files, str):
            files = [files]
        elif not files:
            files = []

        files = [f'{file}.sql' if not file.endswith('.sql') else file for file in files]

        if not os.path.isdir(directory):
            raise NotADirectoryError(f"Did not find the test source directory {self.test_source_dir}")

        for file in sorted(os.listdir(directory)):
            if file.endswith('.sql'):
                if files and (file not in files):
                    continue
                else:
                    with open(os.path.join(directory,file)) as f:
                        yield file.replace(".sql",""), f.read()

    def create_tables(self,names=None):
        """
        Get the CREATE TABLE statements
        :param names: string of list of names of tables to filter
        :return: (filename,statement)
        """
        return self._get_statement(self.create_table_dir, names)

    def drop_tables(self,names=None):
        """
        Get the DROP TABLE statements
        :param names: string of list of names of tables to filter
        :return: (filename,statement)
        """
        for (file, sql) in self.create_tables(names):
            table_name = file.replace('.sql', '')
            yield file, f"drop table if exists {table_name}"

    def create_star_schemas(self,names=None):
        """
        Get the CREATE STAR SCHEMA statements
        :param names: string of list of names of tables to filter
        :return: (filename,statement)
        """
        return self._get_statement(self.create_star_schema_dir, names)

    def drop_star_schemas(self,names=None):
        """
        Get the DROP STAR SCHEMA statements
        :param names: string of list of names of tables to filter
        :return: (filename,statement)
        """
        for (file,sql) in self.create_star_schemas(names):
            table_name = file.replace('.sql','')
            yield file, f"drop star schema on {table_name}"

    def create_olap_indexes(self,names=None):
        """
        Get the CREATE OLAP INDEX statements
        :param names: string of list of names of tables to filter
        :return: (filename,statement)
        """
        return self._get_statement(self.create_olap_index_dir, names)

    def drop_olap_indexes(self,names=None):
        """
        Get the DROP OLAP INDEX statements

        *Important* assumes index name is `<fact-table-name>_snap`

        :param names: string of list of names of tables to filter
        :return: (filename,statement)
        """
        for (file, sql) in self.create_olap_indexes(names):
            table_name = file.replace('.sql', '')
            yield file, f"drop olap index {table_name} on {table_name.replace('_snap','')}"

    def queries(self,names=None):
        """
        Get the SQL Query statements
        :param names: string of list of names of tables to filter
        :return: (filename,statement)
        """
        return self._get_statement(self.sql_dir, names)



if __name__ == "__main__":

    sources = TestSources("../snap-e2e-tests/tpcds")

    files = [
        sources.create_tables(),
        sources.create_star_schemas(),
        sources.create_olap_indexes(),
        sources.queries(),
        sources.drop_tables(),
        sources.drop_star_schemas(),
        sources.drop_olap_indexes(),
        ]

    for gens in files:
        assert list(gens) != []
