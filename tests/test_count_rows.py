import pytest, allure
import config

from spark_locust.tools import generate_sources
from spark_locust.tools.fixtures import execute

sources = generate_sources.TestSources(source_dir=config.TEST_SOURCES)

@allure.title("Count Table Rows")
@pytest.mark.parametrize('name', [name for (name,query) in sources.create_tables()])
def test_count_rows(execute,name):

    execute(name, f"select count(*) as {name} from {name}"
            ,do_benchmark=False
            ,check_expected=True
            ,do_screenshot=False)
