import pytest
from spark_locust.tools.screenshots import NoSandboxChromeBrowser
from spark_locust.tools.fixtures import connection

@pytest.fixture(scope='session')
def splinter_browser_class(request):
    """Browser class to use for browser instance creation."""
    return NoSandboxChromeBrowser
