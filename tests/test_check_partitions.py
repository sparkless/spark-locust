import pytest, allure
import config

from spark_locust.tools import generate_sources

sources = generate_sources.TestSources(source_dir=config.TEST_SOURCES)

@allure.title("Collect Partition")
@pytest.mark.parametrize('name', [name for (name,query)
                                  in sources.create_tables()
                                  if "PARTITIONED BY" in query.upper()]
                         )
def test_msck_repair(connection, name):

    cursor = connection.cursor()
    for f, q in sources.create_tables(name):
        cursor.execute(f"MSCK REPAIR TABLE {f}")
    cursor.close()