import pytest, allure
import config

from spark_locust.tools import generate_sources

sources = generate_sources.TestSources(source_dir=config.TEST_SOURCES)

@allure.title("Drop Table")
@pytest.mark.parametrize('name', [name for (name,query) in sources.drop_tables()])
def test_drop_tables(connection,name):
    cursor = connection.cursor()
    for f, q in sources.drop_tables(name):
        cursor.execute(q)
    cursor.close()
