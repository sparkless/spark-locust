import pytest, allure, os
import config

from spark_locust.tools import generate_sources

sources = generate_sources.TestSources(source_dir=config.TEST_SOURCES)

# path of the stored data
data_url=os.getenv("DATA_URL","file:///tmp")

@allure.title("Create Table")
@pytest.mark.parametrize('name', [name for (name,query) in sources.create_tables()])
def test_create_tables(connection,name):
    cursor = connection.cursor()
    cursor.execute(f'set spark.tpcds.parquet.data.folder={data_url}')
    for f, q in sources.create_tables(name):
        cursor.execute(q)
    cursor.close()
