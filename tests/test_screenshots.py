from spark_locust.tools.screenshots import get_jobs, get_query, get_queries, get_job\
    , screenshot_last_completed_job\
    , screenshot_last_completed_query

def test_get_completed_jobs(browser):
    assert(list(get_jobs(browser)))

def test_get_completed_sql(browser):
    assert(list(get_queries(browser)))

def test_get_query(browser):
    """
    Getting the last SQL/execution page
    :param browser:
    :return:
    """
    last_query_id = next(get_queries(browser))
    print(f'Getting query {last_query_id}')
    query = get_query(browser,qid=last_query_id)
    assert(query.value)

def test_get_job(browser):
    """
    Getting the last job/jobs page
    :param browser:
    :return:
    """
    last_job_id, last_job_name = next(get_jobs(browser))
    print(f'Getting job {last_job_id}')
    job = get_job(browser,jid=last_job_id)

    assert(job)

def test_screenshot_last_job(browser):
    """
    Getting screenshot of last completed job
    :param browser:
    :return:
    """
    id, image = screenshot_last_completed_job(browser)
    print(id)
    assert(id)

def test_screenshot_last_query(browser):
    """
    Getting screenshot of last completed job
    :param browser:
    :return:
    """
    id, image = screenshot_last_completed_query(browser)
    print(id)
    assert (id)

