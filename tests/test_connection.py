
from spark_locust.tools.fixtures import run_query, connection
from config import logger

def test_connection(connection):
    logger.info("Getting databases")
    result = run_query(connection,'show databases')
    assert(result.to_dict())
