import pytest, allure
import config

from spark_locust.tools import generate_sources
from spark_locust.tools.fixtures import execute

sources = generate_sources.TestSources(source_dir=config.TEST_SOURCES)

@allure.title("Run Query")
@pytest.mark.parametrize('name', [name for (name,query) in sources.queries()])
def test_queries(execute,name):

    file, query = list(sources.queries(name))[0]

    execute(name, query
            ,do_benchmark=False
            ,check_expected=True
            ,do_screenshot=True)
