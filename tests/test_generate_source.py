from spark_locust.tools import generate_sources
import config

def test_check_directories_not_empty():

    sources = generate_sources.TestSources(config.TEST_SOURCES)

    files = [
        sources.create_tables(),
        sources.create_star_schemas(),
        sources.create_olap_indexes(),
        sources.queries(),
        sources.drop_tables(),
        sources.drop_star_schemas(),
        sources.drop_olap_indexes(),
        ]

    for gens in files:
        assert list(gens) != []