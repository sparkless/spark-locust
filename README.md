# Spark-E2E : End-2-End Testing for Apache Spark

Spark-Locust is a complete set of tools for performance testing of Apache Spark applications

## Usage

### Setup

1. Edit the `.env` file to change the default configuration
2. Pull the `spark-lucust` Docker image: `make pull-image`
3. Test the connection to the Thriftserver: `./bin/start-spark-locust.sh tests/test_connection.py`

# Running Tests

Each file under the `tests` directory contains a simple test that you can copy to create new tests.

* Create all tables `./bin/start-spark-locust.sh tests/test_create_tables.py`
* Check table partitions `./bin/start-spark-locust.sh tests/test_check_partitions.py`
* Count table rows `./bin/start-spark-locust.sh tests/test_count_rows.py`
* Run Queries `./bin/start-spark-locust.sh tests/test_queries.py`
* Drop tables `./bin/start-spark-locust.sh tests/test_drop_tables.py`

After each test has completed, `spark-locust` will generate a report and serve it on http://localhost:8082

## Features

* Support for Kubernetes
* Benchmark Testing - run performance benchmarks (i.e TPCDS)
* Correctness Testing - automatically checks for correct query results
* Query Plan Capture - automatically captures Spark Query plans
* **WIP** Test Plan Generation - analyzes tests and generates additional test cases
* **WIP** Load Testing - simulate multiple concurrent users
* **WIP** Regression Testing - ensure performance does not decrease across versions

## Kubernetes

* The `charts` directory contains a [Helm Chart](https://helm.sh/) that will launch
`spark-locust` on you k8s cluster.
* Copy and edit `charts/values.yaml` to change the default configuration
* Install the chart with `helm install --name spark-locust charts/spark-locust --values <your-values.yaml>` 

## Limitations

* Only supports SparkSQL through HiveThriftServer.

# Architecture

* This repository ties together various pytest plugins and tools -- see the `Dockerfile` and `requirements.txt`
* Spark Locust will run in a Docker container -- see `bin/start-spark-locust.sh`.
* The `TEST_SOURCES`, `TEST_OUTPUTS` and `EXPECTED_RESULTS` are shared between the container and the host.
* All configurations are passed into the container as environment variables.

# Code Organization

* tools/generate_sources.py - a simple module that extracts SQL files from directory
* tools/screenshots.py - handles capturing of Spark Web UI pages
* tools/fixtures.py - pytest fixtures for screenshots and jdbc connection management
