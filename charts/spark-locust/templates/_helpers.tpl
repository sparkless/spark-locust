{{/* vim: set filetype=mustache: */}}
{{/*
Create a short app name.
*/}}
{{- define "chart.name" -}}
spark-locust
{{- end -}}

{{/*
Create a fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "chart.fullname" -}}
{{- if (.Values.global) and .Values.global.fullnameOverride -}}
{{- .Values.global.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := include "chart.name" . -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the subchart label.
*/}}
{{- define "chart.subchart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{* Spark locust Variables *}
{{- define "chart.spark-locust.hostname" -}}
spark-locust
{{- end -}}

{{- define "chart.spark-locust.name" -}}
{{- template "chart.name" . -}}-spark-locust
{{- end -}}

{{- define "chart.spark-locust.fullname" -}}
{{- $fullname := include "chart.fullname" . -}}
{{- if contains "spark-locust" $fullname -}}
{{- printf "%s" $fullname -}}
{{- else -}}
{{- printf "%s-spark-locust" $fullname | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}