all: spark
spark: build push clean
.PHONY: build push clean

SHELL := /bin/bash

IMAGE_TAG = 0.0.1
IMAGE_NAME = spark-locust
REGISTRY = registry.gitlab.com/sparkless
REPO = spark-locust
VALUES = charts/spark-locust/values.yaml

helm-install:
	helm install --name $(IMAGE_NAME) charts/spark-locust --values $(VALUES)

helm-del:
	helm delete $(IMAGE_NAME) --purge

pull-image:
	docker pull $(REGISTRY)/$(REPO)/$(IMAGE_NAME):$(IMAGE_TAG)

build-image:
	docker build -t $(REGISTRY)/$(REPO)/$(IMAGE_NAME) .
	docker tag $(REGISTRY)/$(REPO)/$(IMAGE_NAME) $(REGISTRY)/$(REPO)/$(IMAGE_NAME):$(IMAGE_TAG)

push-image:
	docker push $(REGISTRY)/$(REPO)/$(IMAGE_NAME)
	docker push $(REGISTRY)/$(REPO)/$(IMAGE_NAME):$(IMAGE_TAG)

clean-image:
	docker rmi $(REGISTRY)/$(REPO)/$(IMAGE_NAME):$(IMAGE_TAG) || :
	docker rmi $(REGISTRY)/$(REPO)/$(IMAGE_NAME) || :

