FROM python:3.6-stretch

# install google chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
RUN apt-get update && \
    apt-get install -y unzip libsasl2-dev vim  libsasl2-modules \ 
        openjdk-8-jdk xvfb libxi6 libgconf-2-4 libnss3-dev google-chrome-stable chromium

 # install chromedriver
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
RUN unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/

# Install spark into $SPARK_HOME
ENV SPARK_VERSION 2.2.1
ENV SPARK_HOME /opt/spark
ENV JAVA_MAJOR_VERSION 8
ENV JAVA_HOME=/usr/lib/jvm/java-$JAVA_MAJOR_VERSION-openjdk-amd64/

RUN rm -rf /var/lib/apt/lists/* && rm -rf /var/cache/

RUN mkdir -p $SPARK_HOME
RUN wget https://archive.apache.org/dist/spark/spark-${SPARK_VERSION}/spark-${SPARK_VERSION}-bin-hadoop2.7.tgz -O - | tar xvz --strip-components=1 -C $SPARK_HOME

# Install allure
ARG RELEASE=2.12.1
ARG ALLURE_REPO=https://dl.bintray.com/qameta/maven/io/qameta/allure/allure-commandline

RUN wget --no-verbose -O /tmp/allure-$RELEASE.zip $ALLURE_REPO/$RELEASE/allure-commandline-$RELEASE.zip \
  && unzip /tmp/allure-$RELEASE.zip -d / \
  && rm -rf /tmp

RUN chmod -R +x /allure-$RELEASE/bin
ENV ALLURE_HOME=/allure-$RELEASE
ENV PATH=$PATH:$ALLURE_HOME/bin

ENV DEFAULT_USER spark-locust
ENV NB_UID 1000
ENV NB_HOST spark-lucust

RUN adduser --gecos '' --shell /bin/bash --uid $NB_UID --disabled-password $DEFAULT_USER

# Install python modules for oci
RUN pip install oci-cli

COPY . /home/$DEFAULT_USER/spark-locust

# For some reason there is no /tmp
RUN mkdir -p /tmp
RUN chown -R $DEFAULT_USER /home/$DEFAULT_USER /usr/local/ ${SPARK_HOME} /tmp

USER $DEFAULT_USER
WORKDIR /home/$DEFAULT_USER

RUN pip install -e /home/$DEFAULT_USER/spark-locust

ENV PATH=$PATH:/home/$DEFAULT_USER/spark-locust

ENV PYTHONDONTWRITEBYTECODE=1
CMD pytest tests 
